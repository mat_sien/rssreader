package pl.sienczykm.rssreader;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Created by sienczykm on 2015-12-21.
 */
public class AutoSyncHandler {

    private static final String TAG = "AutoSyncHandler";

    private AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    private Context context;

    private static final int ALARM_TYPE = AlarmManager.ELAPSED_REALTIME_WAKEUP;

    private static AutoSyncHandler instance = null;

    public static synchronized AutoSyncHandler getInstance(Context context) {
        if (instance == null) {
            instance = new AutoSyncHandler(context);
        }

        return instance;
    }

    private AutoSyncHandler(Context context) {
        this.context = context;
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent updateIntent = new Intent(context, GetFeedService.class);
        updateIntent.putExtra(GetFeedService.UPDATE_ALL, true);
        updateIntent.putExtra(GetFeedService.CODE_KEY, GetFeedService.GET_ITEM);
        pendingIntent = PendingIntent.getService(context, 0, updateIntent, 0);
    }

    public void setNextUpdate() {

        if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean(Config.AUTO_UPDATE, true)) {
            int delay = PreferenceManager.getDefaultSharedPreferences(context).getInt(Config.INTERVAL, Config.TEN_MINUTES);

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                alarmManager.set(ALARM_TYPE, SystemClock.elapsedRealtime() + delay, pendingIntent);
                Log.i(TAG, "Alarm set - next update in: " + delay / 60000 + " min");
            } else {
                alarmManager.setExact(ALARM_TYPE, SystemClock.elapsedRealtime() + delay, pendingIntent);
                Log.i(TAG, "Alarm set exact - next update in: " + delay / 60000 + " min");
            }

        } else {
            cancelUpdate();
        }
    }

    public void cancelUpdate() {
        alarmManager.cancel(pendingIntent);
        Log.i(TAG, "Alarm cancelled");
    }

}
