package pl.sienczykm.rssreader;

/**
 * Created by sienczykm on 2015-12-18.
 */
public class Config {

    public static final String[] DEFAULT_FEEDS = {
            "http://www.filmweb.pl/feed/news/type/news",
            "http://www.gry-online.pl/rss/news.xml",
            "http://pclab.pl/xml/rss.xml",
            "http://www.purepc.pl/rss_all.xml",
            "http://www.tvn24.pl/najnowsze.xml",
            "http://www.cdaction.pl/rss_newsy.xml"
    };

    public static final String FIRST_RUN_KEY = "firstTime";
    public static final String INTERVAL = "interval";
    public static final String AUTO_UPDATE = "autoUpdate";

    public static final int TEN_MINUTES = 1000 * 60 * 10;

}
