package pl.sienczykm.rssreader.db;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by Mateusz on 13.12.2015.
 */
@DatabaseTable(tableName = "item")
public class ItemModel {

    public static final int IS_UNREAD = 0;
    public static final int IS_READ = 1;

    @DatabaseField(generatedId = true)
    private int _id;

    @DatabaseField(canBeNull = false, foreign = true)
    private FeedModel feedId;

    @DatabaseField(uniqueCombo = true)
    private String title;

    @DatabaseField(uniqueCombo = true)
    private String link;


    @DatabaseField
    private long pubDate;

    @DatabaseField
    private String description;

    @DatabaseField
    private int isRead;

    public ItemModel() {
        // ORMLite needs a no-arg constructor
    }

    public FeedModel getFeedId() {
        return feedId;
    }

    public void setFeedId(FeedModel feedId) {
        this.feedId = feedId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public long getPubDate() {
        return pubDate;
    }

    public void setPubDate(long pubDate) {
        this.pubDate = pubDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int isRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }
}
