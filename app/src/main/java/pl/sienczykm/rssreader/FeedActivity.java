package pl.sienczykm.rssreader;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.support.OrmLiteCursorLoader;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Callable;

import pl.sienczykm.rssreader.db.DbHelper;
import pl.sienczykm.rssreader.db.FeedModel;
import pl.sienczykm.rssreader.db.ItemModel;

public class FeedActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = "FeedActivity";

    private static final String UPDATE_DIALOG_VISIBILITY = "dialogVisibility";

    public static final String FROM_NOTIFICATION_KEY = "fromNotification";

    private static final int FEED_LOADER = 2;
    static final int ADD_FEED_REQUEST = 1;  // The request code

    private DbHelper dbHelper;
    private Dao<FeedModel, Integer> feedDao;
    private Dao<ItemModel, Integer> itemDao;
    private OrmLiteCursorLoader<FeedModel> loader;
    private RelativeLayout numberPickerLayout;
    private NumberPicker numberPicker;
    private CheckBox checkBox;
    private LinearLayout numberPickerDialog;

    private ListView feedList;
    private FeedAdapter feedAdapter;
    private boolean numberPickerVisible;

    private SharedPreferences preferences;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            int status = intent.getIntExtra(GetFeedService.REFRESH_STATUS, 69);
            switch (status) {
                case GetFeedService.NO_CONNECTION:
                    Toast.makeText(FeedActivity.this, R.string.no_connection, Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "onCreate");

        setContentView(R.layout.feed_activity);

        dbHelper = getDbHelper();

        try {
            feedDao = dbHelper.getFeedDao();
            itemDao = dbHelper.getItemDao();
        } catch (Exception e) {
            e.printStackTrace();
        }

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isFirstRun = preferences.getBoolean(Config.FIRST_RUN_KEY, true);
        if (isFirstRun) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(Config.FIRST_RUN_KEY, false);
            editor.putInt(Config.INTERVAL, Config.TEN_MINUTES);
            editor.putBoolean(Config.AUTO_UPDATE, true);
            editor.apply();
        }

        numberPickerLayout = (RelativeLayout) findViewById(R.id.numberPickerLayout);
        numberPicker = (NumberPicker) findViewById(R.id.numberPicker);
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(60);
        numberPicker.setWrapSelectorWheel(true);

        checkBox = (CheckBox) findViewById(R.id.cbAutoUpdate);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()) {
                    showNumberPicker();
                } else {
                    hideNumberPicker();
                }
            }
        });
        numberPickerDialog = (LinearLayout) findViewById(R.id.up);

        getSupportLoaderManager().initLoader(FEED_LOADER, null, this);

        feedList = (ListView) findViewById(R.id.feedList);
        feedAdapter = new FeedAdapter(this, null, 0);

        feedList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String url = feedAdapter.getCursor().getString(feedAdapter.getCursor().getColumnIndexOrThrow("url"));
                int feedId = feedAdapter.getCursor().getInt(feedAdapter.getCursor().getColumnIndexOrThrow("_id"));
                String title = feedAdapter.getCursor().getString(feedAdapter.getCursor().getColumnIndexOrThrow("title"));

                Intent intent = new Intent(FeedActivity.this, ItemActivity.class);
                intent.putExtra(ItemActivity.URL_KEY, url);
                intent.putExtra(ItemActivity.FEED_ID, feedId);
                intent.putExtra(ItemActivity.TITLE, title);
                startActivity(intent);
            }
        });

        registerForContextMenu(feedList);

        feedList.setAdapter(feedAdapter);

        if (savedInstanceState != null) {
            numberPickerVisible = savedInstanceState.getBoolean(UPDATE_DIALOG_VISIBILITY);
        } else {
            numberPickerVisible = false;
        }

        if (numberPickerVisible) {
            showUpdateDialog();
        }

        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class
                    .getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            Log.e(TAG, "SDK < 10");
        }

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getBoolean(FROM_NOTIFICATION_KEY, false)) {

            Intent intent = new Intent(this, ItemActivity.class);
            intent.putExtra(ItemActivity.URL_KEY, extras.getString(ItemActivity.URL_KEY));
            intent.putExtra(ItemActivity.FEED_ID, extras.getInt(ItemActivity.FEED_ID));
            intent.putExtra(ItemActivity.TITLE, extras.getString(ItemActivity.TITLE));
            startActivity(intent);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter(GetFeedService.UPDATE_STATUS));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        if (item.getItemId() == R.id.context_delete) {

            Cursor clickedItem = (Cursor) feedAdapter.getItem(info.position);

            try {
                deleteFeed(clickedItem);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return true;
        }

        return super.onContextItemSelected(item);
    }

    private void deleteFeed(Cursor clickedItem) throws Exception {
        int feedId = clickedItem.getInt(clickedItem.getColumnIndexOrThrow("_id"));

        QueryBuilder<FeedModel, Integer> qb = feedDao.queryBuilder();
        qb.where().idEq(feedId);
        List<FeedModel> feedList = qb.query();

        feedDao.delete(feedList.get(0));

        QueryBuilder<ItemModel, Integer> qbItem = itemDao.queryBuilder();
        qbItem.where().eq("feedId_id", feedId);
        final List<ItemModel> allRows = qbItem.query();

        itemDao.callBatchTasks(new Callable<Void>() {
            public Void call() throws Exception {
                for (ItemModel rows : allRows) {
                    itemDao.delete(rows);
                }
                return null;
            }
        });
        Log.i(TAG, "Rows deleted: " + allRows.size());
    }

    private DbHelper getDbHelper() {
        if (dbHelper == null) {
            dbHelper = OpenHelperManager.getHelper(this, DbHelper.class);
        }
        return dbHelper;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.add_feed) {
            Intent intent = new Intent(this, AddFeedActivity.class);
            startActivityForResult(intent, ADD_FEED_REQUEST);
            return true;
        }
        if (id == R.id.settings) {
            showUpdateDialog();

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(UPDATE_DIALOG_VISIBILITY, numberPickerVisible);
        super.onSaveInstanceState(outState);
    }

    private void showUpdateDialog() {
        numberPickerVisible = true;
        checkBox.setChecked(preferences.getBoolean(Config.AUTO_UPDATE, true));
        numberPicker.setValue(millisToMin(preferences.getInt(Config.INTERVAL, Config.TEN_MINUTES)));
        numberPickerLayout.setVisibility(View.VISIBLE);
        if (checkBox.isChecked()) {
            showNumberPicker();
        } else {
            hideNumberPicker();
        }
    }

    public void hideUpdateDialog(View view) {
        numberPickerVisible = false;
        numberPickerLayout.setVisibility(View.GONE);
    }

    public void hideUpdateDialog() {
        hideUpdateDialog(null);
    }

    private void showNumberPicker() {
        numberPickerDialog.setVisibility(View.VISIBLE);
    }

    private void hideNumberPicker() {
        numberPickerDialog.setVisibility(View.GONE);
    }

    public void setInterval(View view) {

        int delay = minToMillis(numberPicker.getValue());
        boolean isChecked = checkBox.isChecked();

        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(Config.INTERVAL, delay);
        editor.putBoolean(Config.AUTO_UPDATE, isChecked);
        editor.apply();

        hideUpdateDialog();

        if (isChecked) {
            AutoSyncHandler.getInstance(this).setNextUpdate();
        } else {
            AutoSyncHandler.getInstance(this).cancelUpdate();
        }

    }

    @Override
    public void onBackPressed() {
        if (numberPickerVisible) {
            hideUpdateDialog();
        } else {
            super.onBackPressed();
        }
    }

    private int minToMillis(int value) {
        return value * 60000;
    }

    private int millisToMin(int value) {
        return value / 60000;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == ADD_FEED_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                feedAdapter.notifyDataSetChanged();
            } else if (resultCode == RESULT_CANCELED) {
                Log.i(TAG, "canceled");
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, Bundle args) {
        switch (loaderId) {
            case FEED_LOADER:
                try {

                    return loader = new OrmLiteCursorLoader<FeedModel>(this, feedDao, feedDao.queryBuilder().prepare());
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            default:
                // An invalid id was passed in
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        feedAdapter.changeCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
   /*
     * Clears out the adapter's reference to the Cursor.
     * This prevents memory leaks.
     */
        feedAdapter.changeCursor(null);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dbHelper != null) {
            OpenHelperManager.releaseHelper();
            dbHelper = null;
        }
    }
}
