package pl.sienczykm.rssreader.db;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Mateusz on 13.12.2015.
 */
@DatabaseTable(tableName = "feed")
public class FeedModel {

    @DatabaseField(generatedId = true)
    private int _id;

    @DatabaseField(uniqueCombo = true)
    private String title;

    @DatabaseField(uniqueCombo = true)
    private String link;

    @DatabaseField
    private String url;

    @DatabaseField
    private String description;

    public FeedModel() {
        // ORMLite needs a no-arg constructor
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }
}
