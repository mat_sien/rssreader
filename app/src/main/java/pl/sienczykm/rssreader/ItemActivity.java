package pl.sienczykm.rssreader;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.support.OrmLiteCursorLoader;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Callable;

import pl.sienczykm.rssreader.db.DbHelper;
import pl.sienczykm.rssreader.db.ItemModel;

/**
 * Created by sienczykm on 2015-12-16.
 */
public class ItemActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "ItemActivity";

    private static final int ITEM_LOADER = 1;
    public static final String URL_KEY = "url";
    public static final String FEED_ID = "feedId";
    public static final String TITLE = "title";

    private SwipeRefreshLayout swipeRefreshLayout;
    private DbHelper dbHelper;
    private Dao<ItemModel, Integer> itemDao;
    private OrmLiteCursorLoader<ItemModel> loader;

    private ListView itemList;
    private ItemAdapter itemAdapter;

    private String url;
    private int feedId;
    private String title;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            int status = intent.getIntExtra(GetFeedService.REFRESH_STATUS, 69);
            switch (status) {
                case GetFeedService.START_REFRESHING:
                    swipeRefreshLayout.setRefreshing(true);
                    break;
                case GetFeedService.STOP_REFRESHING:
                    swipeRefreshLayout.setRefreshing(false);
                    break;
                case GetFeedService.NO_CONNECTION:
                    Toast.makeText(ItemActivity.this, R.string.no_connection, Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_activity);


        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent), getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);

        url = getIntent().getExtras().getString(URL_KEY);
        feedId = getIntent().getExtras().getInt(FEED_ID);
        title = getIntent().getExtras().getString(TITLE);

        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).cancel(feedId);

        setTitle(title);

        onRefresh();

        dbHelper = getDbHelper();

        try {
            itemDao = dbHelper.getItemDao();
        } catch (Exception e) {
            e.printStackTrace();
        }

        getSupportLoaderManager().initLoader(ITEM_LOADER, null, this);

        itemList = (ListView) findViewById(R.id.itemList);
        itemAdapter = new ItemAdapter(this, null, 0);

        itemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Cursor clickedItem = (Cursor) itemAdapter.getItem(position);

                int itemId = clickedItem.getInt(clickedItem.getColumnIndexOrThrow("_id"));
                String itemUrl = clickedItem.getString(clickedItem.getColumnIndexOrThrow("link"));

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(itemUrl));
                startActivity(intent);

                UpdateBuilder<ItemModel, Integer> updateBuilder = itemDao.updateBuilder();
                try {
                    updateBuilder.updateColumnValue("isRead", ItemModel.IS_READ);
                    updateBuilder.where().idEq(itemId);
                    updateBuilder.update();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        itemList.setAdapter(itemAdapter);

    }

    private DbHelper getDbHelper() {
        if (dbHelper == null) {
            dbHelper = OpenHelperManager.getHelper(this, DbHelper.class);
        }
        return dbHelper;
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter(GetFeedService.UPDATE_STATUS));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    @Override
    public void onRefresh() {
        Intent intent = new Intent(this, GetFeedService.class);
        intent.putExtra(GetFeedService.CODE_KEY, GetFeedService.GET_ITEM);
        intent.putExtra(GetFeedService.URL_KEY, url);
        startService(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dbHelper != null) {
            OpenHelperManager.releaseHelper();
            dbHelper = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.item_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.delete_items:
                new AlertDialog.Builder(this)
                        .setTitle(R.string.delete_items)
                        .setMessage(R.string.delete_items_message)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    deleteNews();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                dialog.cancel();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteNews() throws Exception {
        QueryBuilder<ItemModel, Integer> qb = itemDao.queryBuilder();
        qb.where().eq("feedId_id", feedId);
        final List<ItemModel> allRows = qb.query();

        itemDao.callBatchTasks(new Callable<Void>() {
            public Void call() throws Exception {
                for (ItemModel rows : allRows) {
                    itemDao.delete(rows);
                }
                return null;
            }
        });
        Log.i(TAG, "Rows deleted: " + allRows.size());
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, Bundle args) {

        switch (loaderId) {
            case ITEM_LOADER:
                try {
                    QueryBuilder<ItemModel, Integer> qb = itemDao.queryBuilder();
                    qb.where().eq("feedId_id", feedId);
                    qb.orderBy("pubDate", false);
                    return loader = new OrmLiteCursorLoader<ItemModel>(this, itemDao, qb.prepare());
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            default:
                // An invalid id was passed in
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        itemAdapter.changeCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        /*
     * Clears out the adapter's reference to the Cursor.
     * This prevents memory leaks.
     */
        itemAdapter.changeCursor(null);
    }
}
