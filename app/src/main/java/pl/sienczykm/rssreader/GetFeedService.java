package pl.sienczykm.rssreader;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import pl.sienczykm.rssreader.db.DbHelper;
import pl.sienczykm.rssreader.db.FeedModel;
import pl.sienczykm.rssreader.db.ItemModel;

/**
 * Created by sienczykm on 2015-12-15.
 */
public class GetFeedService extends IntentService {

    private static final String TAG = "GetFeedService";

    private static String TAG_TITLE = "title";
    private static String TAG_LINK = "link";
    private static String TAG_DESCRIPTION = "description";
    private static String TAG_ITEM = "item";
    private static String TAG_PUB_DATE = "pubDate";

    public static final String UPDATE_STATUS = "updateStatus";
    public static final String URL_KEY = "url";
    public static final String CODE_KEY = "getCode";
    public static final String UPDATE_ALL = "updateAll";
    public static final String REFRESH_STATUS = "status";
    private int getCode;

    public static final int GET_FEED = 0;
    public static final int GET_ITEM = 1;

    public static final int START_REFRESHING = 0;
    public static final int STOP_REFRESHING = 1;
    public static final int NO_CONNECTION = 3;

    private DbHelper dbHelper;
    private Dao<FeedModel, Integer> feedDao;
    private Dao<ItemModel, Integer> itemDao;

    private int rowAdded = 0;
    NotificationManager mNotifyManager;

    public GetFeedService() {
        super("GetFeedService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        dbHelper = getDbHelper();

        sendMessage(START_REFRESHING);

        boolean updateAll = intent.getBooleanExtra(UPDATE_ALL, false);
        getCode = intent.getIntExtra(CODE_KEY, 69);


        try {
            feedDao = dbHelper.getFeedDao();
            itemDao = dbHelper.getItemDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (Connectivity.isConnected(this)) {
            if (updateAll) {

                QueryBuilder<FeedModel, Integer> qb = feedDao.queryBuilder();
                List<FeedModel> feedList = null;
                try {
                    feedList = qb.query();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                Log.i(TAG, "Number od feeds: " + feedList.size());

                if (feedList.size() > 0) {

                    for (FeedModel feed : feedList) {

                        rowAdded = 0;

                        try {
                            parseXML(feed.getUrl());

                            if (rowAdded > 0) {

                                Intent notificationIntent = new Intent(this, FeedActivity.class);

                                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                notificationIntent.putExtra(FeedActivity.FROM_NOTIFICATION_KEY, true);
                                notificationIntent.putExtra(ItemActivity.URL_KEY, feed.getUrl());
                                notificationIntent.putExtra(ItemActivity.FEED_ID, feed.get_id());
                                notificationIntent.putExtra(ItemActivity.TITLE, feed.getTitle());

//                                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//                                stackBuilder.addParentStack(ItemActivity.class);
//                                stackBuilder.addNextIntent(notificationIntent);
//
//                                // teraz nie otwiera się aktywność z odpowiednim intentem.
//                                PendingIntent notificationPendingIntent = stackBuilder.getPendingIntent(feed.get_id(), PendingIntent.FLAG_UPDATE_CURRENT);

                                PendingIntent notificationPendingIntent = PendingIntent.getActivity(this, feed.get_id(), notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                                NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                                        .setSmallIcon(R.mipmap.ic_launcher)
                                        .setContentTitle(getString(R.string.app_name))
                                        .setContentText(feed.getTitle() + ": " + rowAdded + " " + getString(R.string.new_items))
                                        .setContentIntent(notificationPendingIntent)
                                        .setAutoCancel(true)
                                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

                                mNotifyManager.notify(feed.get_id(), builder.build());
                            }

                            Log.i(TAG, rowAdded + " new items for channel " + feed.getTitle());

                        } catch (SQLException e) {
                            Log.e(TAG, "SQLException");
                            e.printStackTrace();
                        } catch (ParserConfigurationException e) {
                            Log.e(TAG, "ParserConfigurationException");
                            e.printStackTrace();
                        } catch (SAXException e) {
                            Log.e(TAG, "SAXException");
                            e.printStackTrace();
                        } catch (IOException e) {
                            Log.e(TAG, "IOException");
                            e.printStackTrace();
                        }
                    }

                    AutoSyncHandler.getInstance(this).setNextUpdate();
                } else {
                    AutoSyncHandler.getInstance(this).cancelUpdate();
                }

            } else {

                String url = intent.getStringExtra(URL_KEY);

                try {
                    parseXML(url);
                } catch (SQLException e) {
                    Log.e(TAG, "SQLException");
                    e.printStackTrace();
                } catch (ParserConfigurationException e) {
                    Log.e(TAG, "ParserConfigurationException");
                    e.printStackTrace();
                } catch (SAXException e) {
                    Log.e(TAG, "SAXException");
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.e(TAG, "IOException");
                    e.printStackTrace();
                }

                AutoSyncHandler.getInstance(this).setNextUpdate();
            }
        } else {
            Log.e(TAG, "No Internet connection.");
            sendMessage(NO_CONNECTION);
        }

        sendMessage(STOP_REFRESHING);

        if (dbHelper != null) {
            OpenHelperManager.releaseHelper();
            dbHelper = null;
        }
    }

    private void sendMessage(int status) {
        Intent intent = new Intent(UPDATE_STATUS);
        intent.putExtra(REFRESH_STATUS, status);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private DbHelper getDbHelper() {
        if (dbHelper == null) {
            dbHelper = OpenHelperManager.getHelper(this, DbHelper.class);
        }
        return dbHelper;
    }

    private void parseXML(String url) throws ParserConfigurationException, IOException, SAXException, SQLException {
        Log.i(TAG, "Update for: " + url);

        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.setRequestMethod("GET");

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

        Document xmlDoc = documentBuilder.parse(connection.getInputStream());
        Element rootElement = xmlDoc.getDocumentElement();

        FeedModel feed = getOrCreateFeed(rootElement, url);

        NodeList itemList = rootElement.getElementsByTagName(TAG_ITEM);

        for (int i = 0; i < itemList.getLength(); i++) {

            if (itemList.item(i).getNodeType() == Node.ELEMENT_NODE) {

                Element itemElement = (Element) itemList.item(i);

                try {
                    fillItems(feed, itemElement);
                } catch (SQLException e) {
                }
            }
        }
    }

    private FeedModel getOrCreateFeed(Element element, String url) throws SQLException {

        String title = getTextContent(element, TAG_TITLE);
        String link = getTextContent(element, TAG_LINK);
        String desc = getTextContent(element, TAG_DESCRIPTION);

        switch (getCode) {
            case GET_FEED:
                FeedModel feed = new FeedModel();
                feed.setTitle(title);
                feed.setLink(link);
                feed.setDescription(desc);
                feed.setUrl(url);
                feedDao.create(feed);
                return feed;
            case GET_ITEM:
                QueryBuilder<FeedModel, Integer> qb = feedDao.queryBuilder();
                qb.where().eq("title", title).and().eq("link", link);
                List<FeedModel> feedList = qb.query();
                return feedList.get(0);
            default:
                throw new IllegalArgumentException();
        }
    }

    private void fillItems(FeedModel feed, Element element) throws SQLException {

        String title = getTextContent(element, TAG_TITLE);
        String link = getTextContent(element, TAG_LINK);
        long pubDate = getDateFromText(element, TAG_PUB_DATE);
        String desc = getTextContent(element, TAG_DESCRIPTION);

        desc = desc.replaceAll("<.*>", "");

        ItemModel item = new ItemModel();
        item.setFeedId(feed);
        item.setTitle(title);
        item.setLink(link);
        item.setPubDate(pubDate);
        item.setDescription(desc);
        item.setIsRead(ItemModel.IS_UNREAD);
        itemDao.create(item);

        rowAdded++;
    }

    private String getTextContent(Element element, String xmlTag) {
        return element.getElementsByTagName(xmlTag).item(0).getTextContent();
    }

    private long getDateFromText(Element element, String xmlTag) {

        // Sat, 07 Sep 2002 00:00:01 GMT, according to RFC 822
        SimpleDateFormat inputFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);

        Date date = null;
        try {
            date = inputFormat.parse(element.getElementsByTagName(xmlTag).item(0).getTextContent());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long dateInMillis = 0;
        if (date != null) {
            dateInMillis = date.getTime();
        }

        return dateInMillis;
    }

}
