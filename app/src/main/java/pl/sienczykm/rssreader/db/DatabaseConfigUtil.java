package pl.sienczykm.rssreader.db;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by sienczykm on 2015-12-13.
 */
public class DatabaseConfigUtil extends OrmLiteConfigUtil {

    private static final Class<?>[] CLASSES = new Class[]{
            FeedModel.class,
            ItemModel.class
    };

    public static void main(String[] args) throws IOException, SQLException {

        writeConfigFile("ormlite_config.txt", CLASSES);

    }
}
