package pl.sienczykm.rssreader.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import pl.sienczykm.rssreader.R;

/**
 * Created by sienczykm on 2015-12-13.
 */
public class DbHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "RSS.db";
    private static final int DATABASE_VERSION = 1;

    private Dao<FeedModel, Integer> feedDao = null;
    private Dao<ItemModel, Integer> itemDao = null;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {

        try {
            TableUtils.createTable(connectionSource, FeedModel.class);
            TableUtils.createTable(connectionSource, ItemModel.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, FeedModel.class, true);
            TableUtils.dropTable(connectionSource, ItemModel.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Dao<FeedModel, Integer> getFeedDao() throws SQLException {
        if (feedDao == null) {
            feedDao = getDao(FeedModel.class);
        }
        return feedDao;
    }

    public Dao<ItemModel, Integer> getItemDao() throws SQLException {
        if (itemDao == null) {
            itemDao = getDao(ItemModel.class);
        }
        return itemDao;
    }
}
