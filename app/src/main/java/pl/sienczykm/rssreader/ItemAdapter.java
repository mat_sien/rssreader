package pl.sienczykm.rssreader;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import pl.sienczykm.rssreader.db.ItemModel;


/**
 * Created by sienczykm on 2015-12-16.
 */
public class ItemAdapter extends CursorAdapter {

//    private static final LayoutTransition LAYOUT_TRANSITION = new LayoutTransition();

    public ItemAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.item_row, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView tvDate = (TextView) view.findViewById(R.id.date);
        TextView tvTitle = (TextView) view.findViewById(R.id.title);
        TextView tvDesc = (TextView) view.findViewById(R.id.desc);
        ImageView ivNew = (ImageView) view.findViewById(R.id.newImage);

        long longDate = cursor.getLong(cursor.getColumnIndex("pubDate"));
        String title = cursor.getString(cursor.getColumnIndex("title"));
        String desc = cursor.getString(cursor.getColumnIndex("description"));
        int isRead = cursor.getInt(cursor.getColumnIndex("isRead"));

        Date date = new Date(longDate);
        SimpleDateFormat outputDate = new SimpleDateFormat("dd MMM yyyy HH:mm");
        String stringDate = outputDate.format(date);

//        boolean dateEquals = tvDate.getText().equals(stringDate);
//        boolean titleEquals = tvTitle.getText().equals(title);
//        boolean descEquals = tvDesc.getText().equals(desc);
//        boolean needTransition = dateEquals && titleEquals && descEquals;
//
//        ((LinearLayout) view.findViewById(R.id.newImage)).setLayoutTransition(needTransition ? LAYOUT_TRANSITION : null);
//        Log.e("dd", needTransition + " " + title);

        if (isRead == ItemModel.IS_UNREAD) {
            ivNew.setVisibility(View.VISIBLE);
        } else {
            ivNew.setVisibility(View.GONE);
        }

        tvDate.setText(stringDate);
        tvTitle.setText(title);
        tvDesc.setText(desc);
    }
}
