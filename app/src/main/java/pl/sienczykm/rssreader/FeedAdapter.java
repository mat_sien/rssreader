package pl.sienczykm.rssreader;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * Created by sienczykm on 2015-12-16.
 */
public class FeedAdapter extends CursorAdapter {

    public FeedAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.feed_row, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView tvTitle = (TextView) view.findViewById(R.id.feed);

        String title = cursor.getString(cursor.getColumnIndex("title"));

        tvTitle.setText(title);
    }
}
