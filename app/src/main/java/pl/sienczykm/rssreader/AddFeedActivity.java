package pl.sienczykm.rssreader;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by sienczykm on 2015-12-16.
 */
public class AddFeedActivity extends AppCompatActivity {

    private EditText link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_rss_activity);

        link = (EditText) findViewById(R.id.edAddLink);

        link.append("http://");
        link.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }


    public void addFeed(View view) {

        String url = link.getText().toString();
        if (Patterns.WEB_URL.matcher(url).matches()) {
            Intent intent = new Intent(this, GetFeedService.class);
            intent.putExtra(GetFeedService.CODE_KEY, GetFeedService.GET_FEED);
            intent.putExtra(GetFeedService.URL_KEY, url);
            startService(intent);

            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        } else {
            Toast.makeText(this, R.string.bad_url, Toast.LENGTH_SHORT).show();
        }
    }

    public void cancel(View view) {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

    public void addDefault(View view) {
        for (String url : Config.DEFAULT_FEEDS) {
            Intent intent = new Intent(this, GetFeedService.class);
            intent.putExtra(GetFeedService.CODE_KEY, GetFeedService.GET_FEED);
            intent.putExtra(GetFeedService.URL_KEY, url);
            startService(intent);
        }
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
