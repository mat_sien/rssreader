package pl.sienczykm.rssreader;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by sienczykm on 2015-12-18.
 */
public class Connectivity extends BroadcastReceiver {

    private static boolean firstConnect = true;

    public static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    public static boolean isConnected(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected());
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if (isConnected(context)) {
            if (firstConnect) {
                startUpdateService(context);
                firstConnect = false;
            }
        } else {
            firstConnect = true;
        }

        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            startUpdateService(context);
        }
    }

    private void startUpdateService(Context context) {
        AutoSyncHandler.getInstance(context).setNextUpdate();
    }
}
